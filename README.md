# Perrine bot

Perrine bot is a discord bot.
She's rather generic for the moment but I'll try to add things in the future

## Installation

You need NodeJS in order to install and run the bot. I assume you are already familiar with it.

Just `npm install`.

## Configuration

You will need a discord account that will serve as the bot.

Configuration is done in the `config.json` file. The authentication informations are in the `auth` section of the file.

```javascript
"auth":
{
	"email": "bot@email.here",
	"password": "password"
}
```

## Usage

Run the bot with node: `node bot.js`.

Once connected, the basic commands start with a !. e.g. `!ping`.

### Picture command

The `!picture` command fetches a picture in a folder and sends it to the channel.
The default picture folder is `./files`, which has to be created first for the command to work.

## Adding commands

You can add command in the commands.js file.
Commands are objects with a name (string) and a func (function).

The function will take an array of strings, a username and a message, and will return an object containing the message or file to be returned.

## Licence

This project is licensed under the WTFPL (full text in the LICENSE file).

Copyright © 2016 Mériadec d'Armorique
