'use strict';

var logger = require("winston");
var DiscordBot = require("discord.io");

const commands = require('./commands'),
	  hiddenCommands = require('./hidden_commands');

// Logging config
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
	colorize: true
});
logger.level = 'debug';

// Get auth info
var config = require("./config.json");

var bot = new DiscordBot({
	email: config.auth.email,
	password: config.auth.password,
	autorun: true
});

function sendResponse(bot, channelID, res) {
	switch(res.rtype) {
		case 'message':
			logger.debug("Sending message: " + res.message);
			bot.sendMessage({
				to: channelID,
				message: res.message
			}, function(err, response) {
				if (err) {
					logger.error("Failed to send message: " + res.message);
				}
			});
			break;
		case 'file':
			logger.debug("Sending file: " + res.file);
			bot.uploadFile({
				to: channelID,
				file: res.file,
				filename: res.file,
				message: res.message,
				typing: true
			}, function(err, response) {
				if (err) {
					logger.error("Failed to send file: " + res.file);
					sendResponse(bot, channelID, {
						rtype: 'message',
						message: "Sorry, but I can't seem to be able to send you the file..."
					});
				}
			});
			break;
		case 'nothing':
			break;
		default:
			break;
	}

}

bot.on('ready', function(rawEvent) {
	logger.info("Connected");
	logger.info("Logged in as: ");
	logger.info(bot.username + " - (" + bot.id + ")");
});

bot.on('message', function(user, userID, channelID, message, rawEvent) {

	// Regular commands
	if (message.substring(0,1) == config.command.identifier) {
		let args = message.substring(1).split(" "),
			command = args[0];
		args = args.splice(1);

		for (let c = 0; c < commands.length; c++) {
			if (command == commands[c].name) {
				let res = commands[c].func(args, user, message);
				sendResponse(bot, channelID, res);
				return;
			}
		}
	}

	// Hidden commands
	else if (new RegExp("@"+bot.username).test(message)) {

		for (let c = 0; c < hiddenCommands.length; c++) {
			if (hiddenCommands[c].regex.test(message)) {
				let res = hiddenCommands[c].func(user, message);
				sendResponse(bot, channelID, res);
				return;
			}
		}
	}
});
