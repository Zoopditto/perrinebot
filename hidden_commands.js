'use strict';

var config = require('./config.json');

const hiddenCommands = [
{
	regex: /love you/i,
	func: function(user, message) {
		let ans = (user == config.command.owner)
			? "I love you too"
			: "Hehe"
		return {
			rtype: 'message',
			message: ans
		}
	}
}
];

module.exports = hiddenCommands;
