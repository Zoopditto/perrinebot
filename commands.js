'use strict';

var fs = require('fs');

var config = require('./config.json')

const defaultErrorMessage = {
    rtype: 'message',
    message: "I'm afraid I don't have what you're looking for."
};

const commands = [
{
	name: "help",
	desc: "Displays all the commands",
	func: function(args, user, message) {
		let coms = "My commands are: ";
		for (let i = 0; i < commands.length; i++) {
			coms += "\n`" + commands[i].name + "`: " + commands[i].desc;
		}
		return {
			rtype: 'message',
			message: coms
		};
	}
},
	// Simply responds with a pong
{
	name: "ping",
	desc: "Responds a simple message to the user",
	func: function (args, user, message) {
		return {
			rtype: 'message',
			message: "Oui " + user + ", this is Perrine desu wa!"
		};
	}
},
	// Looks for a random picture in the directory specified by the first
	// argument and sends it
	// If no argument is given the directory is picked at random too
{
	name: "picture",
	desc: "Gives a random picture from a selected folder",
	func: function (args, user, message) {
		let dir = "";
		let dirs = fs.readdirSync(config.folders.pictures);
		if (dirs.length == 0) {
			return defaultErrorMessage;
		}
		if (args.length == 0) {
			dir = dirs[Math.floor(Math.random() * dirs.length)];
		}
		else {
			dir = args[0];
			if (dirs.indexOf(dir) == -1) {
				return defaultErrorMessage;
			}
		}
		var pics = fs.readdirSync(config.folders.pictures + '/' + dir);
		if (pics.length == 0) {
			return defaultErrorMessage;
		}
		var pic = pics[Math.floor(Math.random() * pics.length)];
		var fullpath = config.folders.pictures + '/' + dir + '/' + pic;
		return {
			rtype: 'file',
			message: 'Here you go desu wa!',
			file: fullpath
		};
	},
},
	// Lists all the picture folders
{
	name: "list",
	desc: "Displays the current picture folders available",
	func: function (args, user, message) {
		var folders = "";
		var dirs = fs.readdirSync(config.folders.pictures);
		if (dirs.length == 0) {
			return {
				rtype: 'message',
				message: "Ara ara, I don't seem to have any picture folders..."
			};
		}
		for (var i = 0; i < dirs.length; i++) {
			folders += "\n`" + dirs[i] + "`";
		}
		return {
			rtype: 'message',
			message: "My folders are:" + folders
		};
	}
},
	// Screams in horror
{
	name: "scream",
	desc: "Screams in horror",
	func: function(args, user, message) {
		var scream = "";
		var as = Math.floor(Math.random() * 20) + 5;
		var hs = Math.floor(Math.random() * 4) + 1;
		scream = Array(as + 1).join("A");
		scream += Array(hs + 1).join("H");
		scream += "!!!";

		return {
			rtype: 'message',
			message: scream
		};
	}
}

];

module.exports = commands;
